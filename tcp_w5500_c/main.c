/*
 * TCP_Server_W5500.cpp
 *
 * Created: 29.04.2016 10:43:16
 * Author : dinartal
 */ 

#include <avr/io.h>
#include "Ethernet/socket.h"

#define _WIZCHIP_ 5500
#define _WIZCHIP_IO_MODE_ _WIZCHIP_IO_MODE_SPI_VDM_

void  wizchip_select(void);
void  wizchip_deselect(void);
void  wizchip_write(uint8_t wb);
uint8_t wizchip_read();

int main(void)
{
	//SPI_MasterInit
	/* Set MOSI and SCK output, all others input */
	DDRB|= (1<<DDRB2)|(1<<DDRB1)|(1<<DDRB0);
	/* Enable SPI, Master, set clock rate fck/2 */
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<CPOL)|(1<<CPHA)|(0<<SPR0)|(0<<SPR1);
	//SPSR|=(1<<SPI2X);
		
	reg_wizchip_cs_cbfunc(wizchip_select, wizchip_deselect);
	reg_wizchip_spi_cbfunc(wizchip_read, wizchip_write);
    
	while (1) 
    {
    }
}


void  wizchip_select(void)
{
	PORTB&=~(1<<PORTB0);
}

void  wizchip_deselect(void)
{
	PORTB|=(1<<PORTB0);
}

void  wizchip_write(uint8_t wb)
{
	/* Start transmission */
	SPDR = wb;
	/* Wait for transmission complete */
	while(!(SPSR & (1<<SPIF)))
	;
}

uint8_t wizchip_read()
{
	/* Start transmission */
	SPDR = 0x00;
	/* Wait for transmission complete */
	while(!(SPSR & (1<<SPIF)))
	;
	return SPDR;
}